package com.company;

public class Main {

    public static void main(String[] args) {
	     Shape circle = new ShapeBuilder("Circle").selectArea(1).selectColor("Red").selectName("Name").build();
         Shape rectangle = new ShapeBuilder("Rectangle").selectArea(3).selectColor("Yellow").selectName("Name2").build();
         Shape triangle = new ShapeBuilder("Triangle").selectArea(4).selectColor("Green").selectName("Name3").build();

        System.out.println(circle.toString());
        System.out.println(rectangle.toString());
        System.out.println(triangle.toString());
    }
}
